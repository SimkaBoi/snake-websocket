import express from 'express';
import http from 'http';
import WebSocket from 'ws';

import Snakes from './Snakes';
import { ISnake } from '../types';

const INTERVAL = 500;

const app = express();

app.use(express.static('dist/client'));


// initialize a simple http server
const server = http.createServer(app);

// initialize the WebSocket server instance
const wss = new WebSocket.Server({
  server,
  path: '/ws',
});

const snakes = new Snakes();
const connectons: { [id: string]: WebSocket } = {};

setInterval(() => {
  snakes.tick();

  Object.keys(connectons).forEach((id) => {
    const ws: WebSocket = connectons[id];
    const snake = snakes.getSnake(id);
    ws.send(
      JSON.stringify({
        action: 'update',
        id,
        direction: snake && snake.direction || '???',
        color: snake && snake.color || '???',
        snakes: snakes.getSnakes().concat(snakes.getTargets().map<ISnake>((target) => ({
          color: 'green',
          playerId: 'target',
          direction: undefined,
          coordinates: [
            target.coordinate,
          ],
        }))),
      }),
    );
  });
}, INTERVAL);

const deleteSnake = (id: string): void => {
  delete connectons[id];
  snakes.deleteSnake(id);
};

wss.on('connection', (ws: WebSocket): void => {
  const id = snakes.addSnake();
  connectons[id] = ws;

  console.log('[%s] connection created', id);

  // connection is up, let's add a simple simple event
  ws.on('message', (message: string) => {
    // log the received message and send it back to the client
    console.log('[%s] received: %s', id, message);

    try {
      const json = JSON.parse(message);
      switch (json.action) {
        case 'change_direction':
          if (json.direction && snakes.setDirection(id, json.direction)) {
            console.log('[%s] change direction: %s', id, json.direction);
          }
          break;
        default:
          console.log('ERROR-unknown-action', json.action, json);
      }
    } catch (e) {
      console.error(id, e, message);
    }
  });

  ws.on('close', (code: number, reason: string) => {
    console.log('[%s] close || code: %d, reason: %s', id, code, reason);
    deleteSnake(id);
  });

  ws.on('error', (err: Error) => {
    console.log('[%s] error', id, err);
    deleteSnake(id);
  });

  // send immediatly a feedback to the incoming connection
  ws.send(
    JSON.stringify({
      action: 'start',
      id,
      sizeX: snakes.GAME_FIELD_SIZE_X,
      sizeY: snakes.GAME_FIELD_SIZE_Y,
    }),
  );
});

const port = parseInt(String(process.env.PORT || 8999), 10);
// start our server
server.listen(port, '0.0.0.0', () => {
  console.log(`Server started on port ${port} (http://127.0.0.1:${port}/))`);
});

// exit handler for docker
// eslint-disable-next-line @typescript-eslint/no-explicit-any
['SIGINT', 'SIGTERM', 'SIGHUP', 'SIGBREAK'].forEach((signal: any) => {
  process.on(signal, () => {
    console.log(`Received ${signal} - exit...`);
    process.exit();
  });
});
