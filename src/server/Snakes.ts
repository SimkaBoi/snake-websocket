import uuid from 'uuid';
import {
  ISnake, ICoordinate, ITarget, ISnakeDirection,
} from '../types';

export enum SPOT_STATUS {
  FREE,
  HEAD,
  TARGET,
  SNAKE,
}

export default class Snakes {
  public static readonly colors = [
    // "green",
    'red',
    'blue',
    'orange',
    'purple',
    'yellow',
    'BlueViolet',
    'pink',
    'Grey',
    'Teal',
    'Sienna',
  ];

  public readonly GAME_FIELD_SIZE_X = 35;

  public readonly GAME_FIELD_SIZE_Y = 60;

  private snakes: { [id: string]: ISnake } = {};

  private targets: { [id: string]: ITarget } = {};

  public tick(): void {
    Object.keys(this.snakes).forEach((id) => {
      const snake: ISnake = this.snakes[id];

      const [oldFirstCord] = snake.coordinates;
      if (oldFirstCord === undefined) {
        this.deleteSnake(id);
        return;
      }
      const firstCord = {
        x: oldFirstCord.x,
        y: oldFirstCord.y,
      };
      if (snake.direction === 'down') {
        firstCord.x += 1;
      }
      if (snake.direction === 'up') {
        firstCord.x -= 1;
      }
      if (snake.direction === 'left') {
        firstCord.y -= 1;
      }
      if (snake.direction === 'right') {
        firstCord.y += 1;
      }

      //
      if (firstCord.y < 0) {
        firstCord.y = this.GAME_FIELD_SIZE_Y - 1;
      } else if (firstCord.y >= this.GAME_FIELD_SIZE_Y) {
        firstCord.y = 0;
      }

      if (firstCord.x < 0) {
        firstCord.x = this.GAME_FIELD_SIZE_X - 1;
      } else if (firstCord.x >= this.GAME_FIELD_SIZE_X) {
        firstCord.x = 0;
      }

      let snakeLength = snake.coordinates.length;
      const spotStatus = this.getSpotStatus(firstCord);
      if (spotStatus.status === SPOT_STATUS.TARGET && spotStatus.id) {
        delete this.targets[spotStatus.id];
        snakeLength += 1;
        console.log('[%s] +1 snake length', id);
      }

      snake.coordinates = [firstCord].concat(snake.coordinates).slice(0, snakeLength);

      // console.log(id, snake.cords);
      this.snakes[id] = snake;
    });

    Object.keys(this.snakes).forEach((id) => {
      const snake: ISnake = this.snakes[id];

      const [firstCord] = snake.coordinates;

      const spotStatus = this.getSpotStatus(firstCord);
      if (spotStatus.status === SPOT_STATUS.SNAKE && spotStatus.id) {
        snake.coordinates = [];
        console.log('[%s] killed - hit snake', id);
      }

      this.snakes[id] = snake;
    });

    // eslint-disable-next-line no-restricted-syntax, guard-for-in
    for (const targetId in this.targets) {
      // eslint-disable-next-line operator-assignment
      this.targets[targetId].expires = this.targets[targetId].expires - 1;
      if (this.targets[targetId].expires < 0) {
        delete this.targets[targetId];
      }
    }

    // add target avg after 10 ticks
    if (Math.floor(Math.random() * 10) === 3) {
      const freeSpot = this.getFreeSpot();
      if (freeSpot) {
        const targetId = uuid();
        this.targets[targetId] = {
          id: targetId,
          coordinate: freeSpot,
          expires: 50,
        };
      }
    }
  }

  public getSnake(id: string): ISnake | undefined {
    const o = this.snakes[id];
    if (!o) {
      return undefined;
    }
    // clone object
    return { ...o };
  }

  public getSnakes(): ISnake[] {
    return Object.values(this.snakes);
  }

  public getTargets(): ITarget[] {
    return Object.values(this.targets);
  }

  public getSpotStatus(coordinate: ICoordinate): { status: SPOT_STATUS; id?: string } {
    // eslint-disable-next-line no-restricted-syntax, guard-for-in
    for (const snakeId in this.snakes) {
      const { coordinates } = this.snakes[snakeId];
      let hit = SPOT_STATUS.HEAD;
      // eslint-disable-next-line no-restricted-syntax, guard-for-in
      for (const cId in coordinates) {
        if (coordinates[cId].x === coordinate.x && coordinates[cId].y === coordinate.y) {
          return {
            status: hit,
            id: snakeId,
          };
        }
        hit = SPOT_STATUS.SNAKE;
      }
    }

    // eslint-disable-next-line no-restricted-syntax, guard-for-in
    for (const targetId in this.targets) {
      if (this.targets[targetId].coordinate.x === coordinate.x && this.targets[targetId].coordinate.y === coordinate.y) {
        return {
          status: SPOT_STATUS.TARGET,
          id: targetId,
        };
      }
    }
    return {
      status: SPOT_STATUS.FREE,
    };
  }

  public deleteSnake(id: string): void {
    delete this.snakes[id];
  }

  public setDirection(id: string, direction: ISnakeDirection): boolean {
    if (this.snakes[id]) {
      this.snakes[id].direction = direction;
      return true;
    }
    return false;
  }

  public addSnake(): string {
    const id = uuid();
    const freeSpot = this.getFreeSpot();
    if (!freeSpot) {
      throw new Error('no space left to add snake');
    }
    this.snakes[id] = {
      color: this.getRandomColor(),
      playerId: id,
      direction: 'right',
      coordinates: [
        freeSpot,
      ],
    };
    return id;
  }

  // eslint-disable-next-line class-methods-use-this
  public getRandomColor(): string {
    return Snakes.colors[Math.floor(Math.random() * Snakes.colors.length)];
  }

  public getRandomSpot(): ICoordinate {
    return {
      x: Math.floor(Math.random() * (this.GAME_FIELD_SIZE_X - 1)),
      y: Math.floor(Math.random() * (this.GAME_FIELD_SIZE_Y - 1)),
    };
  }

  public getFreeSpot(): ICoordinate | false {
    let i = 250;
    let coordinate: ICoordinate = this.getRandomSpot();
    while (i-- > 0) {
      if (this.getSpotStatus(coordinate).status === SPOT_STATUS.FREE) {
        return coordinate;
      }
      coordinate = this.getRandomSpot();
    }
    return false;
  }
}
