# snakes-websocket

```sh
# install node packages
npm ci

# run everything with hot reload
npm start

#
# test with docker
#

docker build -t snakes .
docker run --rm --name snakes -p 80:80 snakes
# open browser http://localhost:80/
```
